﻿
using Microsoft.AspNetCore.Http;

namespace xposapi.DataModels
{
    public partial class VMTblMProduct
    {

        public int Id { get; set; }
        public string ProductName { get; set; } = null!;
        public decimal? Price { get; set; }
        public int? Stock { get; set; }
        public int VariantId { get; set; }
        public string? VariantName { get; set; }
        public string? Image { get; set; }
        public IFormFile? ImageFile { get; set; }
        public bool? IsDeleted { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public int? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}
