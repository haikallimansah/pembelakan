﻿using Newtonsoft.Json;
using System.Net;
using System.Text;
using xposapi.DataModels;

namespace xposweb.Models
{
    public class VariantsModels
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly string apiMaster;
        private string? jsonData;
        private HttpContent? content;

        public VariantsModels(string _apiMaster)
        {
            apiMaster = _apiMaster;
        }

        public List<VMTblMVariant>? GetAll()
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Variants").Result;

                return JsonConvert.DeserializeObject<List<VMTblMVariant>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMVariant? GetById(int id)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Variants/Get/{id}").Result;

                return JsonConvert.DeserializeObject<VMTblMVariant>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }


        public VMTblMVariant Create(VMTblMVariant data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Variants", content).Result;

                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMVariant?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }


            }
            catch (Exception ex)
            {
                //logging

                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);
               
            }
        }

        //public VMTblMVariant Edit (VMTblMVariant data)
        //{

        //}
    }
}
