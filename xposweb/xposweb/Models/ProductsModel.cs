﻿using Newtonsoft.Json;
using System.Drawing.Printing;
using System.Net;
using System.Text;
using xposapi.DataModels;

namespace xposweb.Models
{
    public class ProductsModel
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly string apiMaster;
        private string? jsonData;
        private HttpContent? content;
        private IWebHostEnvironment webHostEnv;

        public ProductsModel(string _apiMaster, IWebHostEnvironment _webHostEnv)
        {
            apiMaster = _apiMaster;
            webHostEnv = _webHostEnv;

        }

        public List<VMTblMProduct>? GetAll()
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products").Result;

                return JsonConvert.DeserializeObject<List<VMTblMProduct>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMProduct? GetById(int id)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products/Get/{id}").Result;

                return JsonConvert.DeserializeObject<VMTblMProduct>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMProduct Create(VMTblMProduct data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Products", content).Result;

                if (apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMProduct?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }


            }
            catch (Exception ex)
            {
                //logging

                Console.WriteLine(ex.Message);
                throw new Exception(ex.Message);

            }
        }

        public string? UploadImage(IFormFile imageFile, string imageFolder)
        {
            string uniqueFileName = string.Empty;

            if(imageFile != null) 
            {
                uniqueFileName = Guid.NewGuid().ToString() + "-" + imageFile.FileName;
                string uploadFolder = webHostEnv.WebRootPath+ "\\" + imageFolder + "\\" + uniqueFileName;
                using (FileStream fileStream = new FileStream(uploadFolder, FileMode.CreateNew))
                {
                    imageFile.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }

        public List<VMTblMProduct>? GetBy(string filter)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/GetBy/{filter}").Result;

                return JsonConvert.DeserializeObject<List<VMTblMProduct>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public List<object>? GetAllPaging(int pageIndex, int pageSize)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Products/GetByPaging/{pageIndex}/{pageSize}").Result;

                return JsonConvert.DeserializeObject<List<object>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

    }
}
