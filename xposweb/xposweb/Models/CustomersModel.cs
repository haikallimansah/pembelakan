﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using xposapi.DataModels;

namespace xposweb.Models
{
    public class CustomersModel : Controller
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly string apiMaster;
        public CustomersModel(string _apiMaster)
        {
            apiMaster = _apiMaster;

        }
        public List<VMTblMCustomer>? GetAll()
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Customers").Result;

                return JsonConvert.DeserializeObject<List<VMTblMCustomer>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
        public VMTblMCustomer? GetById(int id)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Customers/Get/{id}").Result;

                return JsonConvert.DeserializeObject<VMTblMCustomer>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}
