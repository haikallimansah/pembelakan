﻿using Newtonsoft.Json;
using System.Drawing;
using System.Net;
using System.Text;
using xposapi.DataModels;

namespace Xpos.Models
{
    public class CategoriesModels
    {
        private readonly HttpClient httpClient = new HttpClient();
        private readonly string apiMaster;
        private HttpContent content;
        private string jsonData;

        public CategoriesModels(string _apiMaster)
        {
            apiMaster = _apiMaster;
        }

        public List<VMTblMCategory>? GetAll()
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories").Result;

                return JsonConvert.DeserializeObject<List<VMTblMCategory>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
        public List<object>? GetAllPaging(int pageIndex, int pageSize)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/GetByPaging/{pageIndex}/{pageSize}").Result;

                return JsonConvert.DeserializeObject<List<object>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCategory? GetById(int id)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/Get/{id}").Result;

                return JsonConvert.DeserializeObject<VMTblMCategory>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCategory Create(VMTblMCategory data)
        {
            try
            {
                jsonData = JsonConvert.SerializeObject(data);
                content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                var apiResponse = httpClient.PostAsync($"{apiMaster}/Categories", content).Result;
                
                if(apiResponse.StatusCode == HttpStatusCode.Created)
                {
                    return JsonConvert.DeserializeObject<VMTblMCategory?>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception(apiResponse.ToString());
                }


            }
            catch (Exception ex)
            {
                //logging

                Console.WriteLine(ex.Message);
                return null;
            }
        }

        public List<VMTblMCategory>? GetBy(string filter)
        {
            try
            {
                //Panggil API
                var apiResponse = httpClient.GetStringAsync($"{apiMaster}/Categories/GetBy/{filter}").Result;

                return JsonConvert.DeserializeObject<List<VMTblMCategory>>(apiResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }

        public VMTblMCategory? Delete(int id, int userId)
        {
            try
            {
                var apiResponse = httpClient.DeleteAsync($"{apiMaster}/Categories?id={id}&userId={userId}").Result;
                if (apiResponse.StatusCode == HttpStatusCode.OK)
                {
                    return JsonConvert.DeserializeObject<VMTblMCategory>(apiResponse.Content.ReadAsStringAsync().Result);
                }
                else
                {
                    throw new Exception($"{(int)apiResponse.StatusCode} - {apiResponse.ReasonPhrase}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return null;
            }
        }
    }
}