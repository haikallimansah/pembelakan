﻿using Microsoft.AspNetCore.Mvc;
using Xpos.Models;
using xposapi.DataModels;
using xposweb.Models;

namespace xposweb.Controllers
{
    public class VariantsController : Controller
    {
        private VariantsModels variant;
        private CategoriesModels categories;

        public VariantsController(IConfiguration _config)
        {
            variant = new VariantsModels(_config["ApiMasterUrl"]);
            categories = new CategoriesModels(_config["ApiMasterUrl"]);
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Variant List";

            List<VMTblMVariant>? data = variant.GetAll();

            return View(data);
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Variant Details";

            VMTblMVariant? data = variant.GetById(id);

            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.Title = "Create Variant";
            ViewBag.CatData = categories.GetAll();
            return View();

        }

        [HttpPost]
        public VMTblMVariant Add(VMTblMVariant data) => variant.Create(data);


        public IActionResult Edit(int id)
        {
            ViewBag.Title = "Edit Variant";
            VMTblMVariant? data = variant.GetById(id);
            return View(data);
        }
    }
}
