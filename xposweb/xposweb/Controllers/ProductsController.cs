﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Xpos.Models;
using xposapi.DataModels;
using xposweb.AddOns;
using xposweb.Models;

namespace xposweb.Controllers
{
    public class ProductsController : Controller
    {
        private ProductsModel product;
        private VariantsModels variant;
        private CategoriesModels category;
        private readonly string imgFolder;
        private readonly int pageSize;

        public ProductsController(IConfiguration _config, IWebHostEnvironment _webHostEnv)
        {
            product = new ProductsModel(_config["ApiMasterUrl"], _webHostEnv);
            variant = new VariantsModels(_config["ApiMasterUrl"]);
            category = new CategoriesModels(_config["ApiMasterUrl"]);
            imgFolder = _config["ImageFolder"];
            pageSize = int.Parse(_config["PageSize"]);
        }

        public IActionResult Index(string? filter, string? orderBy, int? pageNumber, int? currPageSize)
        {
            List<VMTblMProduct>? data;
            int totalData = 0;

            if (string.IsNullOrEmpty(filter))
            {
                List<object> pagingData = product.GetAllPaging(pageNumber ?? 1, currPageSize ?? pageSize);

                data = JsonConvert.DeserializeObject <List<VMTblMProduct>?>(JsonConvert.SerializeObject(pagingData[0]));
                totalData = JsonConvert.DeserializeObject <int>(JsonConvert.SerializeObject(pagingData[1]));
            }
            else
            {
                data = product.GetBy(filter);
            }

            switch (orderBy)
            {
                case "stock_desc":
                    data = data.OrderByDescending(p => p.Stock).ToList();
                    break;
                case "stock":
                    data = data.OrderBy(p => p.Stock).ToList();
                    break;
                case "price_desc":
                    data = data.OrderByDescending(p => p.Price).ToList();
                    break;
                case "price":
                    data = data.OrderBy(p => p.Price).ToList();
                    break;
                case "productname_desc":
                    data = data.OrderByDescending(p => p.ProductName).ToList();
                    break;
                case "productname":
                    data = data.OrderBy(p => p.ProductName).ToList();
                    break;
                case "id_desc":
                    data = data.OrderByDescending(p => p.Id).ToList();
                    break;
                default:
                    data = data.OrderBy(p => p.Id).ToList();
                    break;
            }


            ViewBag.Title = "Product List";
            ViewBag.ImgFolder = imgFolder;

            //current page
            ViewBag.Filter = filter;
            ViewBag.OrderBy = orderBy;
            ViewBag.PageSize = currPageSize ?? pageSize;

            //update order selection
            ViewBag.orderId = string.IsNullOrEmpty(orderBy) ? "id_desc" : "" ;
            ViewBag.orderName = string.IsNullOrEmpty(orderBy) ? "productname_desc" : "";
            ViewBag.orderPrice = string.IsNullOrEmpty(orderBy) ? "price_desc" : "";
            ViewBag.orderStock = string.IsNullOrEmpty(orderBy) ? "stock_desc" : "";


            return View(Pagination<VMTblMProduct>.Create(data, totalData, pageNumber ?? 1, ViewBag.PageSize));
        }


        public IActionResult Details(int id)
        {
            ViewBag.Title = "Product Details";

            VMTblMProduct? data = product.GetById(id);

            return View(data);
        }

        public IActionResult Create()
        {
            ViewBag.catData = category.GetAll().Select(
                 c => new SelectListItem
                 {
                     Value = c.Id.ToString(),
                     Text = c.CategoryName
                 }).ToList();

            ViewBag.varData = variant.GetAll();

            return View();

        }

        [HttpPost]
        public VMTblMProduct? Add(VMTblMProduct data)
        {
            try
            {
                //upload if image existed
                if (data.ImageFile != null)
                {
                    //upload image process
                    data.Image = product.UploadImage(data.ImageFile, imgFolder);
                    data.ImageFile = null;
                }

                return product.Create(data);
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine(ex.Message);
                throw new Exception(ex.ToString());
            }

        }


    }
}
