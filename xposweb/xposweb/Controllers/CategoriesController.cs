﻿using Microsoft.AspNetCore.Mvc;
using Xpos.Models;
using xposapi.DataModels;

namespace xposweb.Controllers
{
    public class CategoriesController : Controller
    {
        private CategoriesModels category;
            
        public CategoriesController(IConfiguration _config)
        {
            category = new CategoriesModels(_config["ApiMasterUrl"]);
        }

        public IActionResult Index(string filter)
        {
            List<VMTblMCategory>? data;
            if (filter == null)
            {
                 data = category.GetAll();
            }
            else
            {
                data = category.GetBy(filter);
            }
            ViewBag.Title = "Category List";


            return View(data);
        }

        public IActionResult Details(int id)
        {
            ViewBag.Title = "Category Details";

            VMTblMCategory? data = category.GetById(id);

            return View(data);
        }

        public IActionResult Create() 
        {
            ViewBag.Title = "Create Category";
            return View();

        }

        [HttpPost]
        public VMTblMCategory Add(VMTblMCategory data)
        {
            VMTblMCategory newData = category.Create(data);

            return newData;
        }

        public IActionResult Update()
        {
            ViewBag.Title = "Update Category";
            return View();

        }
        [HttpPut]
        public VMTblMCategory? Updated(VMTblMCategory data)
        {
            VMTblMCategory? newData = category.Create(data);

            return newData;

        }

        public IActionResult Delete(int id)
        {
            ViewBag.Title = "Delete Category";
            return View(id);
        }
        [HttpDelete]
        public VMTblMCategory? Deleted(VMTblMCategory data) => category.Delete(data.Id, (int)data.UpdateBy!);




    }
}
