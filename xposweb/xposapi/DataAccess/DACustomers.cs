﻿using Microsoft.EntityFrameworkCore.Storage;
using xposapi.DataModels;

namespace xposapi.DataAccess
{
    public class DACustomers
    {
        private readonly pembekalan1Context db;
        public DACustomers(pembekalan1Context _db)
        {
            db = _db;
        }

        public List<VMTblMCustomer>? GetAll() => GetByFilter(string.Empty);

        public List<VMTblMCustomer>? GetByFilter(string? filter)
        {
            try
            {
                List<VMTblMCustomer>? data =
                (
                    from cu in db.TblMCustomers
                    where cu.IsDeleted == false && (cu.Name.Contains(filter))
                    select new VMTblMCustomer
                    {
                        Id = cu.Id,
                        Name = cu.Name,
                        Email = cu.Email,
                        Password = cu.Password,
                        Address = cu.Address,
                        Phone = cu.Phone,
                        IsDeleted = cu.IsDeleted,
                        CreateBy = cu.CreateBy,
                        CreateDate = cu.CreateDate,
                        UpdateBy = cu.UpdateBy,
                        UpdateDate = cu.UpdateDate
                    }
                ).ToList();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public VMTblMCustomer? GetById(int id)
        {
            try
            {
                VMTblMCustomer? data =
                (
                    from cu in db.TblMCustomers
                    where cu.IsDeleted == false && cu.Id == id
                    select new VMTblMCustomer
                    {
                        Id = cu.Id,
                        Name = cu.Name,
                        Email = cu.Email,
                        Password = cu.Password,
                        Address = cu.Address,
                        Phone = cu.Phone,
                        IsDeleted = cu.IsDeleted,
                        CreateBy = cu.CreateBy,
                        CreateDate = cu.CreateDate,
                        UpdateBy = cu.UpdateBy,
                        UpdateDate = cu.UpdateDate
                    }
                ).FirstOrDefault();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public TblMCustomer CreateUpdate(VMTblMCustomer input)
        {
            TblMCustomer newData = new TblMCustomer();
            newData.Name = input.Name;
            newData.Email = input.Email;
            newData.Password = input.Password;
            newData.Address = input.Address;
            newData.Phone = input.Phone;
            newData.IsDeleted = (input.IsDeleted ?? false);

            using(IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (input.Id == 0 || input.Id == null)
                    {
                        //Create New Customer
                        newData.CreateBy = input.CreateBy;
                        newData.CreateDate = input.CreateDate;

                        db.Add(newData);

                    }
                    else
                    {
                        //Check existing data
                        VMTblMCustomer? existingCustomer = GetById(input.Id);

                        if (existingCustomer != null)
                        {
                            //Update existing Customer
                            newData.Id = input.Id;
                            newData.CreateBy = existingCustomer.CreateBy;
                            newData.CreateDate = existingCustomer.CreateDate;

                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;

                            db.Update(newData);

                        }
                        else
                        {
                            throw new Exception($"Customer with id = {input.Id} invalid");
                        }

                    }

                    //Execute query
                    db.SaveChanges();

                    //Commit Db Changes
                    dbTran.Commit();
                }
                catch(Exception ex)
                {
                    //Rollback DB Changes
                    dbTran.Rollback();

                    //Logging
                    Console.WriteLine(ex.Message);
                }
            }
            return newData;
        }
    }
}

