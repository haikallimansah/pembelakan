﻿using Microsoft.EntityFrameworkCore.Storage;
using xposapi.DataModels;

namespace xposapi.DataAccess
{
    public class DACategories
    {
        private readonly pembekalan1Context db;
        public DACategories(pembekalan1Context _db)
        {
            db = _db;
        }

        public VMTblMCategory? GetById(int id)
        {
            try
            {
                VMTblMCategory? data =
                (
                    from c in db.TblMCategories
                    where c.Id == id && c.IsDeleted == false
                    select new VMTblMCategory
                    {
                        Id = c.Id,
                        CategoryName = c.CategoryName,
                        Description = c.Description,
                        CreateBy = c.CreateBy,
                        CreateDate = c.CreateDate,
                        IsDeleted = c.IsDeleted,
                        UpdateBy   = c.UpdateBy,
                        UpdateDate = c.UpdateDate,
                    }
                ).FirstOrDefault();
                return data;
            }
            catch (Exception ex) 
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public List <VMTblMCategory>? GetAll() => GetByFilter(string.Empty);

        public List<VMTblMCategory>? GetByFilter(string? filter)
        {
            try
            {
                List<VMTblMCategory>? data =
                (
                    from c in db.TblMCategories
                    where c.IsDeleted == false && (c.CategoryName.Contains(filter) || c.Description.Contains(filter))
                    select new VMTblMCategory
                    {
                        Id = c.Id,
                        CategoryName = c.CategoryName,
                        Description = c.Description,
                        CreateBy = c.CreateBy,
                        CreateDate = c.CreateDate,
                        IsDeleted = c.IsDeleted,
                        UpdateBy = c.UpdateBy,
                        UpdateDate = c.UpdateDate,
                    }
                ).ToList();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public TblMCategory CreateUpdate(VMTblMCategory input)
        {
            TblMCategory newData = new TblMCategory();
            newData.CategoryName = input.CategoryName;
            newData.Description = input.Description;
            newData.IsDeleted = (input.IsDeleted ?? false);
            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (input.Id == 0 || input.Id == null)
                    {
                        //Create New Category
                        newData.CreateBy = input.CreateBy;
                        newData.CreateDate = input.CreateDate;

                        db.Add(newData);

                    }
                    else
                    {
                        //Check existing data
                        VMTblMCategory? existingCategory = GetById(input.Id);

                        if (existingCategory != null)
                        {
                            //Update existing Category
                            newData.Id = input.Id;
                            newData.CreateBy = existingCategory.CreateBy;
                            newData.CreateDate = existingCategory.CreateDate;

                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;

                            db.Update(newData);

                        }
                        else
                        {
                            throw new Exception($"Category with id = {input.Id} invalid");
                        }

                    }

                    //Execute query
                    db.SaveChanges();

                    //Commit Db Changes
                    dbTran.Commit();
                }
                catch(Exception ex)
                {
                    //Rollback DB Changes
                    dbTran.Rollback();

                    //Logging
                    Console.WriteLine(ex.Message);
                }
            }

            return newData;
        }

    }
 }
