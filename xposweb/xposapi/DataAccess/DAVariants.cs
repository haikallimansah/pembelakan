﻿using Microsoft.EntityFrameworkCore.Storage;
using xposapi.DataModels;
namespace xposapi.DataAccess
{
    public class DAVariants
    {
        private readonly pembekalan1Context db;
        public DAVariants(pembekalan1Context _db)
        {
            db = _db;
        }
        public VMTblMVariant? GetById(int id)
        {
            return (
                from v in db.TblMVariants
                join c in db.TblMCategories
                     on v.CategoryId equals c.Id
                where v.IsDeleted == false && v.Id == id
                select new VMTblMVariant
                {
                    Id = v.Id,
                    Description = v.Description,
                    Name = v.Name,
                    CategoryName = c.CategoryName,
                    IsDeleted = v.IsDeleted,
                    CreateBy = v.CreateBy,
                    CreateDate = v.CreateDate,
                    UpdateDate = v.UpdateDate,

                }
            ).FirstOrDefault();
        }

        public List<VMTblMVariant>? GetAll() => GetByFilter(string.Empty);

        public List<VMTblMVariant>? GetByFilter(string? filter)
        {
            try
            {
                List<VMTblMVariant>? data =
                (
                    from v in db.TblMVariants
                    join c in db.TblMCategories on v.CategoryId equals c.Id
                    where c.IsDeleted == false && (v.Name.Contains(filter) || v.Description.Contains(filter))
                    select new VMTblMVariant
                    {
                        Id = v.Id,
                        Description = v.Description,
                        CategoryId = c.Id,
                        Name = v.Name,
                        CategoryName = c.CategoryName,
                        IsDeleted = v.IsDeleted,
                        CreateBy = v.CreateBy,
                        CreateDate = v.CreateDate,
                        UpdateDate = v.UpdateDate,
                    }
                ).ToList();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public TblMVariant CreateUpdate(VMTblMVariant input)
        {
            TblMVariant newData = new TblMVariant();
            newData.Name = input.Name;
            newData.Description = input.Description;
            newData.CategoryId = input.CategoryId;
            newData.IsDeleted = (input.IsDeleted ?? false);

            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {
                try
                {
                    if (input.Id == 0 || input.Id == null)
                    {
                        //Create New Category
                        newData.CreateBy = input.CreateBy;
                        newData.CreateDate = DateTime.Now;

                        db.Add(newData);

                    }
                    else
                    {
                        //Check existing data
                        VMTblMVariant? existingCategory = GetById(input.Id);

                        if (existingCategory != null)
                        {
                            //Update existing Variant
                            newData.Id = input.Id;
                            newData.CreateBy = existingCategory.CreateBy;
                            newData.CreateDate = existingCategory.CreateDate;

                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;

                            db.Update(newData);

                        }
                        else
                        {
                            throw new Exception($"Variant with id = {input.Id} invalid");
                        }

                    }

                    //Execute query
                    db.SaveChanges();

                    //Commit Db Changes
                    dbTran.Commit();
                }
                catch (Exception ex)
                {
                    //Rollback DB Changes
                    dbTran.Rollback();

                    //Logging
                    Console.WriteLine(ex.Message);
                }
            }
            return newData;
        }





    }
}
