﻿using Microsoft.EntityFrameworkCore.Storage;
using xposapi.DataModels;

namespace xposapi.DataAccess
{
    public class DAProducts
    {
        private readonly pembekalan1Context db;
        private readonly IWebHostEnvironment webHostEnvironment;
        private readonly string imageFolder = "Images";
        public DAProducts(pembekalan1Context _db, IWebHostEnvironment _webHostEnvironment)
        {
            db = _db;
            webHostEnvironment = _webHostEnvironment;
        }

        private string? UploadImage(IFormFile ImageFile)
        {
            string uniqueFileName = string.Empty;
            if (ImageFile != null)
            {
                uniqueFileName = Guid.NewGuid().ToString() + "-" + ImageFile.FileName;

                string uploadFolder = webHostEnvironment.WebRootPath + "\\" + "\\" + uniqueFileName;

                using (FileStream fileStream = new FileStream(uploadFolder, FileMode.CreateNew))
                {
                    ImageFile.CopyTo(fileStream);
                }
            }
            return uniqueFileName;
        }


        public List<VMTblMProduct>? GetAll() => GetByFilter(string.Empty);

        public List<VMTblMProduct>? GetByFilter(string? filter)
        {
            try
            {
                List<VMTblMProduct>? data =
                (
                    from p in db.TblMProducts
                    join v in db.TblMVariants on p.VariantId equals v.Id
                    join c in db.TblMCategories on v.CategoryId equals c.Id
                    where p.IsDeleted == false && (p.Name.Contains(filter))
                    select new VMTblMProduct
                    {
                        Id = p.Id,
                        ProductName = p.Name,
                        Price  = p.Price,
                        Stock = p.Stock,                        
                        VariantId = v.Id,
                        VariantName = v.Name,
                        Image = p.Image,
                        IsDeleted = p.IsDeleted,
                        CreateBy = p.CreateBy,
                        CreateDate = p.CreateDate,
                        UpdateBy = p.UpdateBy,
                        UpdateDate = p.UpdateDate
                    }
                ).ToList();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public VMTblMProduct? GetById(int id)
        {
            try
            {
                VMTblMProduct? data =
                (
                    from p in db.TblMProducts
                    join v in db.TblMVariants on p.VariantId equals v.Id
                    join c in db.TblMCategories on v.CategoryId equals c.Id
                    where p.IsDeleted == false && p.Id == id
                    select new VMTblMProduct
                    {
                        Id = p.Id,
                        ProductName = p.Name,
                        Price = p.Price,
                        Stock = p.Stock,
                        VariantId = v.Id,
                        VariantName = v.Name,
                        Image = p.Image,
                        CreateBy = p.CreateBy,
                        CreateDate = p.CreateDate,
                        IsDeleted = p.IsDeleted,
                        UpdateBy = p.UpdateBy,
                        UpdateDate = p.UpdateDate
                    }
                ).FirstOrDefault();
                return data;
            }

            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Error : " + ex.Message);
                return null;
            }
        }

        public TblMProduct CreateUpdate(VMTblMProduct input)
        {
            TblMProduct newData = new TblMProduct();
            newData.Name = input.ProductName;
            newData.Price = input.Price;
            newData.Stock = input.Stock;
            newData.VariantId = input.VariantId;
            newData.Image = input.Image;
            newData.IsDeleted = (input.IsDeleted ?? false);

            using (IDbContextTransaction dbTran = db.Database.BeginTransaction()) 
            {
                try
                {
                    if (input.Id == 0 || input.Id == null)
                    {
                        //Create New Category
                        newData.CreateBy = input.CreateBy;
                        newData.CreateDate = input.CreateDate;

                        db.Add(newData);

                    }
                    else
                    {
                        //Check existing data
                        VMTblMProduct? existingCategory = GetById(input.Id);

                        if (existingCategory != null)
                        {
                            //Update existing Variant
                            newData.Id = input.Id;
                            newData.CreateBy = existingCategory.CreateBy;
                            newData.CreateDate = existingCategory.CreateDate;

                            newData.UpdateBy = input.UpdateBy;
                            newData.UpdateDate = DateTime.Now;

                            db.Update(newData);

                        }
                        else
                        {
                            throw new Exception($"Variant with id = {input.Id} invalid");
                        }

                    }

                    //Execute query
                    db.SaveChanges();

                    //Commit Db Changes
                    dbTran.Commit();
                }
                catch(Exception ex)
                {
                    //Rollback DB Changes
                    dbTran.Rollback();

                    //Logging
                    Console.WriteLine(ex.Message);
                }
            }

            return newData;
        }
    }
}
