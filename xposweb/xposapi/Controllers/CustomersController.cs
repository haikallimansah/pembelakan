﻿using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;
using xposapi.DataAccess;
using xposapi.DataModels;

namespace xposapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
        private DACustomers customers;

        public CustomersController(pembekalan1Context _db)
        {
            customers = new DACustomers(_db);
        }


        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMCustomer? data = customers.GetById(id);
                if (data != null)
                    return Ok(data);
                else
                    throw new Exception($"Customer with id = {id} not found ");
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                List<VMTblMCustomer>? data = customers.GetAll();

                if (data!.Count > 0)
                    return Ok(data);
                else
                    throw new ArgumentNullException("Customer table has no data!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpGet("[action]/{filter}")]
        public IActionResult GetBy(string filter)
        {
            try
            {
                List<VMTblMCustomer>? data;
                if (!string.IsNullOrEmpty(filter))
                {
                    data = customers.GetByFilter(filter);

                    if (data.Count > 0)
                        return Ok(data);
                    else
                        throw new Exception($"Customer with Name = {filter} does not exist");


                }
                else
                    throw new ArgumentNullException("Please provide parts of Customer Name first!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create(VMTblMCustomer customer)
        {
            try {
               return Created("/api/Customers",customers.CreateUpdate(customer));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Customer API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(VMTblMCustomer customer)
        {
            try
            {
                return Ok(customers.CreateUpdate(customer));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Customer API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId) 
        {
            try
            {
                //get existing data
                VMTblMCustomer? existingCustomer = customers.GetById(id);
                existingCustomer!.IsDeleted = true;
                existingCustomer.UpdateBy = userId;

                return Ok(customers.CreateUpdate(existingCustomer));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Customer API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
