﻿using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;
using xposapi.DataAccess;
using xposapi.DataModels;

namespace xposapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriesController : Controller
    {
        private DACategories categories;

        public CategoriesController(pembekalan1Context _db) 
        {
            categories = new DACategories(_db);
        }


        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id){
            try 
            {
                VMTblMCategory? data = categories.GetById(id);
                if (data != null)
                    return Ok(data);
                else
                    throw new Exception($"Category with id = {id} not found ");
            }

            catch(Exception ex) 
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetAll(){
            try
            {
                List<VMTblMCategory>? data = categories.GetAll();

                if (data!.Count > 0)
                    return Ok(data);
                else
                    throw new ArgumentNullException("Category table has no data!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpGet("[action]/{filter}")]
        public IActionResult GetBy(string filter)
        {
            try
            {
                List<VMTblMCategory>? data;
                if (!string.IsNullOrEmpty(filter))
                {
                    data = categories.GetByFilter(filter);

                    if (data.Count > 0)
                        return Ok(data);
                    else
                        throw new Exception($"Category with Name or Description = {filter} does not exist");


                }
                else
                    throw new ArgumentNullException("Please provide parts of Category Name or Description first!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create(VMTblMCategory category)
        {
            try {
               return Created("/api/Categories",categories.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Category API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(VMTblMCategory category)
        {
            try
            {
                return Ok(categories.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Category API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId) 
        {
            try
            {
                //get existing data
                VMTblMCategory? existingCategory = categories.GetById(id);
                existingCategory!.IsDeleted = true;
                existingCategory.UpdateBy = userId;

                return Ok(categories.CreateUpdate(existingCategory));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Category API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("[action]/{pageIndex?}/{pageSize?}")]
        public IActionResult GetByPage(int pageIndex, int pageSize)
        {
            try
            {
                List<object> result = new List<object>();
                result.Add(categories.GetAll().Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList());
                result.Add(categories.GetAll().Count());

                return Ok(result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Category API: " + ex.Message);

                return BadRequest(ex.Message);
            }
        }


    }
}
