﻿using Microsoft.AspNetCore.Mvc;
using System.Linq.Expressions;
using xposapi.DataAccess;
using xposapi.DataModels;

namespace xposapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProductsController : Controller
    {
        private DAProducts products;

        public ProductsController(pembekalan1Context _db, IWebHostEnvironment _webHostEnvironment)
        {
            products = new DAProducts(_db, _webHostEnvironment);
        }


        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMProduct? data = products.GetById(id);
                if (data != null)
                    return Ok(data);
                else
                    throw new Exception($"Product with id = {id} not found ");
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                List<VMTblMProduct>? data = products.GetAll();

                if (data!.Count > 0)
                    return Ok(data);
                else
                    throw new ArgumentNullException("Product table has no data!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpGet("[action]/{filter}")]
        public IActionResult GetBy(string filter)
        {
            try
            {
                List<VMTblMProduct>? data;
                if (!string.IsNullOrEmpty(filter))
                {
                    data = products.GetByFilter(filter);

                    if (data.Count > 0)
                        return Ok(data);
                    else
                        throw new Exception($"Product with Name = {filter} does not exist");


                }
                else
                    throw new ArgumentNullException("Please provide parts of Product Name first!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet("[action]/{pageIndex?}/{pageSize?}")]
        public IActionResult GetByPaging(int pageIndex, int pageSize) 
        {
            try
            {
                List<VMTblMProduct>? data = products.GetAll();

                if(data.Count() > 0)
                {
                    List<object>? result = new List<object>();
                    result.Add(data.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList());
                    result.Add(data.Count());
                    return Ok(result);
                }
                else
                {
                    throw new Exception("Product has no data!");
                }
            }
            catch(Exception ex)
            {
                //logging
                Console.WriteLine("Product API: " + ex.Message);

                return BadRequest(ex.Message);
            }
        }


        [HttpPost]
        public IActionResult Create(VMTblMProduct category)
        {
            try
            {
                return Created("/api/Products", products.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(VMTblMProduct product)
        {
            try
            {
                return Ok(products.CreateUpdate(product));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMProduct? existingProduct = products.GetById(id);
                existingProduct!.IsDeleted = true;
                existingProduct.UpdateBy = userId;

                return Ok(products.CreateUpdate(existingProduct));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Product API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }
    }
}
