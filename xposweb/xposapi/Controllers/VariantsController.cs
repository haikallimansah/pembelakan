﻿using Microsoft.AspNetCore.Mvc;
using xposapi.DataAccess;
using xposapi.DataModels;

namespace xposapi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VariantsController : Controller
    {
        private DAVariants variants;
        public VariantsController(pembekalan1Context _db)
        {
            variants = new DAVariants(_db);
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblMVariant? data = variants.GetById(id);
                if (data != null)
                    return Ok(data);
                else
                    throw new Exception($"Variant {id} not found ");
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                List<VMTblMVariant>? data = variants.GetAll();

                if (data.Count > 0)
                {
                    return Ok(data);
                }
                else
                    throw new ArgumentNullException("Variant table has no data!");
            }
            catch (Exception ex)
            {
                //Logging
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create(VMTblMVariant category)
        {
            try
            {
                return Created("/api/Variants", variants.CreateUpdate(category));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Variants API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(VMTblMVariant variant)
        {
            try
            {
                return Ok(variants.CreateUpdate(variant));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Variant API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete]
        public IActionResult Delete(int id, int userId)
        {
            try
            {
                //get existing data
                VMTblMVariant? existingCategory = variants.GetById(id);
                existingCategory!.IsDeleted = true;
                existingCategory.UpdateBy = userId;

                return Ok(variants.CreateUpdate(existingCategory));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Variant API : " + ex.Message);
                return BadRequest(ex.Message);
            }
        }

    }
}
