﻿using Microsoft.AspNetCore.Mvc;
using xposapi.DataModels;
using xposapitransaction.DataAccess;
using xposdatamodels;
using xposviewmodels;

namespace xposapitransaction.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly DAOrders orders;
        public OrderController(pembekalan1Context _db)
        {
            orders = new DAOrders(_db) ;
        }

        [HttpGet("[action]/{id?}")]
        public IActionResult Get(int id)
        {
            try
            {
                VMTblTOrder? data = orders.GetById(id);
                if (data != null)
                    return Ok(data);
                else
                    throw new Exception($"Order with id = {id} not found ");
            }

            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            try
            {
                List<VMTblTOrder>? data = orders.GetAll();

                if (data!.Count > 0)
                    return Ok(data);
                else
                    throw new ArgumentNullException("Order table has no data!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);

            }
        }

        [HttpGet("[action]/{filter}")]
        public IActionResult GetBy(string filter)
        {
            try
            {
                List<VMTblTOrder>? data;
                if (!string.IsNullOrEmpty(filter))
                {
                    data = orders.GetByFilter(filter);

                    if (data.Count > 0)
                        return Ok(data);
                    else
                        throw new Exception($"Order with ... or ... = {filter} does not exist");


                }
                else
                    throw new ArgumentNullException("Please provide parts of ... or ... first!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Create(VMTblTOrder order)
        {
            try
            {
                return Created("/api/Orders", orders.CreateUpdate(order));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Orders API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }

        [HttpPut]
        public IActionResult Update(VMTblTOrder order)
        {
            try
            {
                return Ok(orders.CreateUpdate(order));
            }
            catch (Exception ex)
            {
                //logging
                Console.WriteLine("Orders API : " + ex.Message);
                return BadRequest(ex.Message);
            }

        }


    }
}
