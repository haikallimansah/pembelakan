﻿using Microsoft.EntityFrameworkCore.Storage;
using System.Data;
using xposapi.DataModels;
using xposdatamodels;
using xposviewmodels;

namespace xposapitransaction.DataAccess
{
    public class DAOrders
    {
        private readonly pembekalan1Context db;

        public DAOrders(pembekalan1Context _db)
        {
            db = _db;
        }

        private string TrxCodeGenerator(int? lastId)
        {
            string trxNo = (lastId != null || lastId > 0) ? lastId++.ToString().PadLeft(5,'0') : "00001";
            return $"XA-{DateTime.Now.ToString("yyyyMMdd")}-{trxNo}";
        }

        public List<VMTblTOrder>? GetAll() {
            return(
                from oh in db.TblTOrderHeaders
                join c in db.TblMCustomers on oh.CustomerId equals c.Id
                where oh.IsDeleted == false
                select new VMTblTOrder
                {
                    Id = oh.Id,
                    TrxCode = oh.TrxCode,
                    Amount = oh.Amount,
                    TotalQty = oh.TotalQty,
                    IsCheckout = oh.IsCheckout,
                    IsDeleted = oh.IsDeleted,

                    CustomerId = oh.CustomerId,
                    CustomerName = c.Name,

                    CreateBy = oh.CreateBy,
                    CreateDate = oh.CreateDate,
                    UpdateBy = oh.UpdateBy,
                    UpdateDate = oh.UpdateDate,
                    orderDetails = (
                        from od in db.TblTOrderDetails
                        join p in db.TblMProducts on od.ProductId equals p.Id
                        where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                        select new VMTblTOrderDetail
                        {
                            Id = od.Id,
                            OrderHeaderId = od.OrderHeaderId,
                            ProductId = od.ProductId,
                            ProductName = p.Name,
                            Price = od.Price,
                            Qty = od.Qty,

                            IsDeleted = od.IsDeleted,
                            CreateBy = od.CreateBy,
                            CreateDate = od.CreateDate,
                            UpdateBy = od.UpdateBy,
                            UpdateDate = od.UpdateDate,

                        }

                    ).ToList()

                }
                ).ToList();
        }

        public List<VMTblTOrder> GetByFilter(string filter)
        {
            return (
                from oh in db.TblTOrderHeaders
                join c in db.TblMCustomers on oh.CustomerId equals c.Id
                where oh.IsDeleted == false && (c.Name.Contains(filter) || oh.TrxCode.Contains(filter) || oh.Amount == decimal.Parse(filter==""?"0":filter))
                select new VMTblTOrder
                {
                    Id = oh.Id,
                    TrxCode = oh.TrxCode,
                    Amount = oh.Amount,
                    TotalQty = oh.TotalQty,
                    IsCheckout = oh.IsCheckout,
                    IsDeleted = oh.IsDeleted,

                    CustomerId = oh.CustomerId,
                    CustomerName = c.Name,

                    CreateBy = oh.CreateBy,
                    CreateDate = oh.CreateDate,
                    UpdateBy = oh.UpdateBy,
                    UpdateDate = oh.UpdateDate,
                    orderDetails = (
                        from od in db.TblTOrderDetails
                        join p in db.TblMProducts on od.ProductId equals p.Id
                        where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                        select new VMTblTOrderDetail
                        {
                            Id = od.Id,
                            OrderHeaderId = od.OrderHeaderId,
                            ProductId = od.ProductId,
                            ProductName = p.Name,
                            Price = od.Price,
                            Qty = od.Qty,

                            IsDeleted = od.IsDeleted,
                            CreateBy = od.CreateBy,
                            CreateDate = od.CreateDate,
                            UpdateBy = od.UpdateBy,
                            UpdateDate = od.UpdateDate,
                        }
                    ).ToList()
                }
                ).ToList();
        }

        public VMTblTOrder GetById(int id)
        {
            return (
                   from oh in db.TblTOrderHeaders
                   join c in db.TblMCustomers on oh.Id equals c.Id
                   where oh.IsDeleted == false && oh.Id == id
                   select new VMTblTOrder
                   {
                       Id = oh.Id,
                       TrxCode = oh.TrxCode,
                       Amount = oh.Amount,
                       TotalQty = oh.TotalQty,
                       IsCheckout = oh.IsCheckout,
                       IsDeleted = oh.IsDeleted,

                       CustomerId = oh.CustomerId,
                       CustomerName = c.Name,

                       CreateBy = oh.CreateBy,
                       CreateDate = oh.CreateDate,
                       UpdateBy = oh.UpdateBy,
                       UpdateDate = oh.UpdateDate,
                       orderDetails = (
                            from od in db.TblTOrderDetails
                            join p in db.TblMProducts on od.Id equals p.Id
                            where od.OrderHeaderId == oh.Id && od.IsDeleted == false
                            select new VMTblTOrderDetail
                            {
                                Id = od.Id,
                                OrderHeaderId = od.OrderHeaderId,
                                ProductId = od.ProductId,
                                ProductName = p.Name,
                                Price = od.Price,
                                Qty = od.Qty,

                                IsDeleted = od.IsDeleted,
                                CreateBy = od.CreateBy,
                                CreateDate = od.CreateDate,
                                UpdateBy = od.UpdateBy,
                                UpdateDate = od.UpdateDate,
                            }
                        ).ToList()
                   }
                ).FirstOrDefault()!;
        }


        public List<VMTblTOrderDetail> GetOrderDetailByFilter(string filter)
        {
            return(
                   from od in db.TblTOrderDetails
                   join p in db.TblMProducts on od.ProductId equals p.Id
                   where od.IsDeleted == false && p.Name.Contains(filter)
                   select new VMTblTOrderDetail 
                   {
                       Id = od.Id,
                       OrderHeaderId = od.OrderHeaderId,
                       ProductId = od.ProductId,
                       ProductName = p.Name,
                       Price = od.Price,
                       Qty = od.Qty,

                       IsDeleted = od.IsDeleted,
                       CreateBy = od.CreateBy,
                       CreateDate = od.CreateDate,
                       UpdateBy = od.UpdateBy,
                       UpdateDate = od.UpdateDate,
                   }      
                ).ToList();
        }

        public VMTblTOrder CreateUpdate(VMTblTOrder input)
        {
            int newOrderHeaderId = 0;

            using (IDbContextTransaction dbTran = db.Database.BeginTransaction())
            {

                try
                {
                    TblTOrderHeader? orderHeader = new TblTOrderHeader();
                    TblTOrderDetail orderDetail = new TblTOrderDetail();
                    TblMProduct product = new TblMProduct();

                    if (input.Id == 0 || input.Id == null)
                    {
                        orderHeader.Amount = input.Amount;
                        orderHeader.TotalQty = input.TotalQty;
                        orderHeader.CustomerId = input.CustomerId;
                        orderHeader.IsCheckout = input.IsCheckout;
                        orderHeader.IsDeleted = (false);
                        //Create new Order
                        orderHeader.TrxCode = TrxCodeGenerator(
                            db.TblTOrderHeaders
                            .OrderBy(oh => oh.Id)
                            .Select(oh => oh.Id)
                            .LastOrDefault()
                            );

                        orderHeader.CreateBy = input.CreateBy;

                        //Save Order Header Data
                        db.Add(orderHeader);
                        db.SaveChanges();
                        newOrderHeaderId = orderHeader.Id;

                        //Order Details process
                        foreach (VMTblTOrderDetail item in input.orderDetails)
                        {
                            orderDetail = new TblTOrderDetail();
                            orderDetail.OrderHeaderId = orderHeader.Id;

                            orderDetail.ProductId = item.ProductId;
                            orderDetail.Qty = item.Qty;
                            orderDetail.Price = item.Price;
                            orderDetail.IsDeleted = input.IsDeleted ?? false;
                            orderDetail.CreateBy = orderHeader.CreateBy;
                            orderDetail.CreateDate = orderHeader.CreateDate;

                            //Save each Order Detail
                            db.Add(orderDetail);
                            db.SaveChanges();

                            //Update product stock
                            product = new TblMProduct();
                            product = db.TblMProducts.Find(item.ProductId);
                            if (product != null)
                            {
                                //Check if current stock is enough
                                if (product.Stock >= item.Qty)
                                {
                                    product.Stock -= item.Qty;

                                    //Update product stock data
                                    db.Update(product);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    throw new Exception($"Product {product.Name} with Id = {item.Id} doesnt have enough stock ");
                                }
                            }
                            else
                            {
                                throw new Exception($"Product with Id = {item.Id} doesnt exist");
                            }
                        }

                    }
                    else
                    {
                        //Update existing Order
                            //Get existing order by its Order Header Id
                        VMTblTOrder? existingOrder = GetById(input.Id);

                        if (existingOrder != null)
                        {
                            orderHeader.Id = existingOrder.Id;
                            orderHeader.CustomerId = existingOrder.CustomerId;
                            orderHeader.TrxCode = existingOrder.TrxCode;
                            orderHeader.CreateBy = existingOrder.CreateBy;
                            orderHeader.CreateDate = existingOrder.CreateDate;
                            orderHeader.UpdateBy = existingOrder.UpdateBy;
                            orderHeader.UpdateDate = DateTime.Now;
                            orderHeader.Amount = input.Amount;
                            orderHeader.TotalQty = input.TotalQty;
                            orderHeader.IsCheckout = input.IsCheckout;
                            orderHeader.IsDeleted = false;

                            //Update order header data
                            db.Update(orderHeader);
                            db.SaveChanges();

                            //Process each order details
                            foreach(VMTblTOrderDetail item in input.orderDetails)
                            {
                                //get existing orderdetails item
                                VMTblTOrderDetail? existingItem = existingOrder.orderDetails.FirstOrDefault(x => x.Id == item.Id);
                                orderDetail = new TblTOrderDetail();
                                orderDetail.Id = existingItem.Id;
                                orderDetail.OrderHeaderId = item.OrderHeaderId;
                                orderDetail.CreateBy = item.CreateBy;
                                orderDetail.CreateDate = existingItem.CreateDate;
                                orderDetail.IsDeleted = false;

                                orderDetail.ProductId = item.ProductId;
                                orderDetail.Price = item.Price;
                                orderDetail.Qty = item.Qty;
                                orderDetail.UpdateBy = item.UpdateBy;
                                orderDetail.UpdateDate = orderHeader.UpdateDate;

                                //update order details data
                                db.Update(orderDetail);
                                db.SaveChanges();

                                //get existing product data


                                //update product stock
                                product = db.TblMProducts.Find(item.ProductId);
                                if (product != null)
                                {
                                    if(existingItem.Qty > item.Qty)
                                    {
                                        //if item less than before
                                        product.Stock += (existingItem.Qty = item.Qty);
                                    }
                                    else
                                    {
                                        if(product.Stock >= (item.Qty - existingItem.Qty))
                                        {
                                            product.Stock -= (item.Qty - existingItem.Qty);
                                        }
                                        else
                                        {
                                            throw new Exception($"Product {item.ProductName} with id {item.Id} doesnt have enough stock");
                                        }
                                    }

                                    db.Update(product);
                                    db.SaveChanges();
                                }
                                else
                                {
                                    //product doesnt exist
                                    throw new Exception($"Product {item.ProductName} with id {item.Id} doesnt exist");
                                }

                            }

                        }
                        else
                        {
                            //existing order doesnt exist
                            throw new Exception($"Order with with id {input.Id} doesnt exist");

                        }

                    }
                    dbTran.Commit();
                    return GetById(orderHeader.Id)!;
                }
                catch (Exception ex)
                {
                    //Undo changes
                    dbTran.Rollback();

                    //Logging
                    Console.WriteLine(ex.Message);
                    throw new Exception(ex.Message);
                }
            }
        }
    }
}
